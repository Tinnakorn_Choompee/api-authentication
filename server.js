const alias      = require('module-alias/register');
const express    = require('express')
const bodyParser = require('body-parser');
const app        = express()
const port       = 3000

// Headers
app.use(bodyParser.json()); // application/json
app.use(bodyParser.urlencoded({ extended: true })); // x-www-form-urlencoded

const api = require('./routes/api');

app.use('/api', api);

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Server app listening on port ${port}!`));